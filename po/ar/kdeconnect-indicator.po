# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeconnect-kde package.
#
# Zayed Al-Saidi <zayed.alsaidi@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kdeconnect-kde\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-12 00:45+0000\n"
"PO-Revision-Date: 2022-09-12 22:32+0400\n"
"Last-Translator: Zayed Al-Saidi <zayed.alsaidi@gmail.com>\n"
"Language-Team: ar\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"
"X-Generator: Lokalize 21.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "زايد السعيدي"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "zayed.alsaidi@gmail.com"

#: deviceindicator.cpp:53
#, kde-format
msgid "Browse device"
msgstr "تصفّح جهاز"

#: deviceindicator.cpp:67
#, kde-format
msgid "Ring device"
msgstr "رنّ على الجهاز"

#: deviceindicator.cpp:81
#, kde-format
msgid "Get a photo"
msgstr "احصل على صورة"

#: deviceindicator.cpp:101
#, kde-format
msgid "Send a file/URL"
msgstr "أرسل ملفًّا/رابطاً"

#: deviceindicator.cpp:111
#, kde-format
msgid "SMS Messages..."
msgstr "الرسائل النصية القصيرة..."

#: deviceindicator.cpp:124
#, kde-format
msgid "Run command"
msgstr "نفّذ أمرًا"

#: deviceindicator.cpp:126
#, kde-format
msgid "Add commands"
msgstr "أضف أوامر"

#: indicatorhelper_mac.cpp:34
#, kde-format
msgid "Launching"
msgstr "يُطلق"

#: indicatorhelper_mac.cpp:84
#, kde-format
msgid "Launching daemon"
msgstr "مطلق الأوامر"

#: indicatorhelper_mac.cpp:93
#, kde-format
msgid "Waiting D-Bus"
msgstr "ينتظر دي-باص"

#: indicatorhelper_mac.cpp:110 indicatorhelper_mac.cpp:120
#: indicatorhelper_mac.cpp:135
#, kde-format
msgid "KDE Connect"
msgstr "كِيدِي المتّصل"

#: indicatorhelper_mac.cpp:111 indicatorhelper_mac.cpp:121
#, kde-format
msgid ""
"Cannot connect to DBus\n"
"KDE Connect will quit"
msgstr ""
"تعذر الاتصال بدي باص\n"
"كِيدِي المتصل سيخرج"

#: indicatorhelper_mac.cpp:135
#, kde-format
msgid "Cannot find kdeconnectd"
msgstr "لم يعثر على kdeconnectd"

#: indicatorhelper_mac.cpp:140
#, kde-format
msgid "Loading modules"
msgstr "يحمل الوحدات"

#: main.cpp:41
#, kde-format
msgid "KDE Connect Indicator"
msgstr "مؤشر كِيدِي المتّصل"

#: main.cpp:43
#, kde-format
msgid "KDE Connect Indicator tool"
msgstr "أداة مؤشر كِيدِي المتّصل"

#: main.cpp:45
#, kde-format
msgid "(C) 2016 Aleix Pol Gonzalez"
msgstr "© 2016 أليكس بول غونزاليس"

#: main.cpp:80
#, kde-format
msgid "Configure..."
msgstr "اضبط..."

#: main.cpp:102
#, kde-format
msgid "Pairing requests"
msgstr "طلبات الاقتران"

#: main.cpp:107
#, kde-format
msgid "Pair"
msgstr "اقرن"

#: main.cpp:108
#, kde-format
msgid "Reject"
msgstr "ارفض"

#: main.cpp:114 main.cpp:124
#, kde-format
msgid "Quit"
msgstr "أنهِ"

#: main.cpp:143 main.cpp:167
#, kde-format
msgid "%1 device connected"
msgid_plural "%1 devices connected"
msgstr[0] "جهاز متصل"
msgstr[1] "جهاز واحد متصل"
msgstr[2] "جهازان متصلان"
msgstr[3] "%1 أجهزة متصلة"
msgstr[4] "%1 جهازاً متصلا"
msgstr[5] "%1 جهاز متصل"

#: systray_actions/battery_action.cpp:28
#, kde-format
msgid "No Battery"
msgstr "بدون بطارية"

#: systray_actions/battery_action.cpp:30
#, kde-format
msgid "Battery: %1% (Charging)"
msgstr "البطارية: %1٪ (تشحن)"

#: systray_actions/battery_action.cpp:32
#, kde-format
msgid "Battery: %1%"
msgstr "البطارية: %1٪"

#: systray_actions/connectivity_action.cpp:30
#, kde-format
msgctxt ""
"The fallback text to display in case the remote device does not have a "
"cellular connection"
msgid "No Cellular Connectivity"
msgstr "لا يوجد تغطية"

#: systray_actions/connectivity_action.cpp:33
#, kde-format
msgctxt ""
"Display the cellular connection type and an approximate percentage of signal "
"strength"
msgid "%1  | ~%2%"
msgstr "%1  | ~%2%"
